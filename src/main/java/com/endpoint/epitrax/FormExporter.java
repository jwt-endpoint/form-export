package com.endpoint.epitrax;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.cli.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import org.json.simple.parser.ParseException;

public class FormExporter {
    private String[][] queries = {
      {"form", "SELECT * FROM form WHERE id = ?"},
      {"agency", "SELECT a.* FROM agency a LEFT JOIN form f ON f.agency_id = a.id LEFT JOIN form_agency fa ON fa.form_id = f.id AND a.id = fa.agency_id WHERE f.id = ?"},
      {"condition_form", "SELECT c.* FROM condition_form c WHERE c.form_id = ?"},
      {"condition_form_auto", "SELECT c.* FROM condition_form_auto c WHERE c.form_id = ?"},
      {"condition", "SELECT DISTINCT ON (id) * FROM (SELECT c.*, form_id FROM condition c JOIN condition_form_auto cfa ON cfa.condition_id = c.id UNION ALL SELECT c.*, form_id FROM condition c JOIN condition_form cf ON cf.condition_id = c.id) foo WHERE form_id = ?"},
      {"form_agency", "SELECT * FROM form_agency WHERE form_id = ?"},
      {"form_element", "SELECT * FROM form_element WHERE form_id = ?"},
      {"question", "SELECT q.* FROM question q JOIN form_element fe ON fe.id = form_element_id WHERE fe.form_id = ?"}
    };
  public static void main(String[] args) {
    FormExporter f = new FormExporter();

    try {
      f.go(args);
    } catch (Exception e) {
      System.err.println("Ran into exception");
      e.printStackTrace();
      System.exit(1);
    }
  }

  private void go(String[] args) throws ClassNotFoundException, ParseException, SQLException, IOException, org.apache.commons.cli.ParseException {
    Class.forName("org.postgresql.Driver");
    CommandLine cmd = parseCommands(args);
    Connection conn = getConnection(
      cmd.getOptionValue("url", "jdbc:postgresql://localhost:5432/nedss"),
      cmd.getOptionValue("user"), cmd.getOptionValue("pass")
    );

    if (cmd.hasOption("import")) {
      doImport(cmd, conn);
    } else {
      Integer formId = Integer.parseInt(cmd.getOptionValue("id"));

      JSONObject form = new JSONObject();

      // Might be good to null out "form.rolled_back_from" field
      PreparedStatement st;
      ResultSet rs;

      try {
        for (String[] queryDef : queries) {
          st = conn.prepareStatement(queryDef[1]);
          st.setInt(1, formId);
          rs = st.executeQuery();
          form.put(queryDef[0], getJsonResults(rs));
        }
        conn.rollback();
      } finally {
        if (conn != null) { conn.close(); }
      }
      
      System.out.println(form.toString());
    }
  }

  private void doImport(CommandLine cmd, Connection conn) throws FileNotFoundException, IOException, ParseException, SQLException {
    JSONParser parser = new JSONParser();
    HashMap<String, Object> obj = (HashMap<String, Object>) parser.parse(new FileReader(cmd.getOptionValue("file")));

    // Check that the necessary conditions exist, unless --skip-conditions
    if (!cmd.hasOption("skip-conditions")) {
      System.out.println("Conditions");
      PreparedStatement st = conn.prepareStatement("SELECT id FROM condition WHERE id = ?");
      for (Object conditionObj : ((JSONArray) obj.get("condition"))) {
        JSONObject condition = (JSONObject) conditionObj;
        st.setLong(1, (Long) condition.get("id"));
        ResultSet rs = st.executeQuery();
        if (!rs.next()) {
          throw new RuntimeException("Couldn't find condition " + ((String) condition.get("name")));
        } else {
          System.out.println("Found condition " + ((String) condition.get("name")));
          condition.put("new-id", rs.getInt(1));
        }
      }
      // We've found all conditions. TODO: Fill in condition_form and condition_form_auto using new condition IDs
    }

    if (!cmd.hasOption("skip-agencies")) {
      // TODO: Check that agencies exist and do something useful with them
    }

    // Write forms, form elements, and questions
    // TODO: Make sure we create form tables as needed. The triggers should do it for us, I think.
    // TODO: What do we do if there's already a form of that name? Gotta archive
    // the old one, perhaps. Probably warn the user that it might be a bad idea
    // to do that.
    int formId = 0;
    for (Object formObj : ((JSONArray) obj.get("form"))) {
      System.out.println(((JSONObject) formObj).toJSONString());
      JSONObject form = (JSONObject) formObj;
      PreparedStatement st = conn.prepareStatement("INSERT INTO form (event_type, updated_at, is_template, name, description, created_at, template_id, short_name, version, status) VALUES (?, now(), ?, ?, ?, now(), null, ?, ?, ?) RETURNING id");
      // (?, now(), ?, ?, ?, now(), null, ?, ?, ?) RETURNING id")
      st.setObject(1, form.get("event_type"));
      st.setObject(2, form.get("is_template"));
      st.setObject(3, form.get("name"));
      st.setObject(4, form.get("description"));
      st.setObject(5, form.get("short_name"));
      st.setObject(6, form.get("version"));
      st.setObject(7, form.get("status"));
      ResultSet rs = st.executeQuery();
      rs.next();
      formId = rs.getInt("id");
      form.put("new_id", formId);
      System.out.println("Created form " + ((String) form.get("name")) + " with new ID " + rs.getInt("id"));
      break; // There should be only one form in the file anyway
    }

    PreparedStatement st = conn.prepareStatement("INSERT INTO form_element (form_id, type, name, description, lft, rgt, created_at, updated_at, is_template, is_active, code, sort) VALUES (?, ?, ?, ?, ?, ?, now(), now(), ?, ?, ?, ?) RETURNING id");
    for (Object formElemObj : ((JSONArray) obj.get("form_element"))) {
      JSONObject formElement = (JSONObject) formElemObj;
      st.setInt(1, formId);
      st.setObject(2, formElement.get("type"));
      st.setObject(3, formElement.get("name"));
      st.setObject(4, formElement.get("description"));
      st.setObject(5, formElement.get("lft"));
      st.setObject(6, formElement.get("rgt"));
      st.setObject(7, formElement.get("is_template"));
      st.setObject(8, formElement.get("is_active"));
      st.setObject(9, formElement.get("code"));
      st.setObject(10, formElement.get("sort"));
      ResultSet rs = st.executeQuery();
      rs.next();
      formElement.put("new-id", rs.getInt(1));
    }

    st = conn.prepareStatement("INSERT INTO question (form_element_id, question_text, help_text, data_type, size, is_required, created_at, updated_at, core_data, core_data_attr, short_name, style, data_set_type_id) VALUES (?, ?, ?, ?, ?, ?, now(), now(), ?, ?, ?, ?, ?) RETURNING id");
    for (Object questionObj : ((JSONArray) obj.get("question"))) {
      JSONObject question = (JSONObject) questionObj;
      Long questionFormElementId = (Long) question.get("form_element_id");
      Boolean found = false;
      for (Object formElemObj : ((JSONArray) obj.get("form_element"))) {
        JSONObject fe = (JSONObject) formElemObj;
        if (questionFormElementId.equals(((Long) fe.get("id")))) {
          st.setInt(1, (Integer) fe.get("new-id"));
          found = true;
          break;
        }
      }
      if (!found) {
        throw new RuntimeException("Couldn't find form element ID " + questionFormElementId);
      }
      st.setObject(2, question.get("question_text"));
      st.setObject(3, question.get("help_text"));
      st.setObject(4, question.get("data_type"));
      st.setObject(5, question.get("size"));
      st.setObject(6, question.get("is_required"));
      st.setObject(7, question.get("core_data"));
      st.setObject(8, question.get("core_data_attr"));
      st.setObject(9, question.get("short_name"));
      st.setObject(10, question.get("stype"));
      st.setObject(11, question.get("data_set_type_id"));
      ResultSet rs = st.executeQuery();
      rs.next();
      question.put("new-id", rs.getInt(1));
    }
    if (!cmd.hasOption("dry-run")) {
      conn.commit();
    } else {
      System.err.println("--dry-run mode specified. Rolling back.");
      conn.rollback();
    }
  }

  private JSONArray getJsonResults(ResultSet rs) throws SQLException {
    JSONArray arr = new JSONArray();
    while (rs.next()) {
      arr.add(getJsonRow(rs));
    }
    return arr;
  }

  private JSONObject getJsonRow(ResultSet rs) throws SQLException {
    JSONObject result = new JSONObject();
    ResultSetMetaData md = rs.getMetaData();

    for (int i = 1; i <= md.getColumnCount(); i++) {
      // This JSON library doesn't quote date values as strings, and complains
      // when reinterpreting them in the import phase, so we need to make them
      // strings manually
      // NB! This only cares about TIMESTAMP types; other types hopefully won't show up in these data
      if (md.getColumnType(i) == java.sql.Types.TIMESTAMP) {
        Object o = rs.getObject(i);
        if (o == null) {
          result.put(md.getColumnLabel(i), null);
        } else {
          result.put(md.getColumnLabel(i), o.toString());
        }
      } else {
        result.put(md.getColumnLabel(i), rs.getObject(i));
      }
    }
    return result;
  }

  private Connection getConnection(String url, String username, String password) throws SQLException {
    Connection conn = DriverManager.getConnection(url, username, password);
    conn.setAutoCommit(false);
    return conn;
  }

  private void printHelp(Options options) {
      new HelpFormatter().printHelp("java -jar form-exporter.jar --user USER --pass PASS [--import] [--file FILE] [--id ID] [--help] [--skip-conditions] [--skip-agencies] [--url jdbc:postgresql://host:port/database]", options);
      System.exit(1);
  }

  private CommandLine parseCommands(String[] args) throws ParseException, org.apache.commons.cli.ParseException {
    Options options = new Options();
    CommandLine cmd = null;

    try {
      options.addOption(Option.builder().longOpt("url").argName("url").hasArg(true).desc("The JDBC URL for the database to use. Default: jdbc:postgresql://localhost:5432/nedss").build());
      options.addOption(Option.builder().longOpt("user").argName("user").hasArg(true).required().desc("The database username; required").build());
      options.addOption(Option.builder().longOpt("pass").argName("pass").hasArg(true).required().desc("The database password; required").build());
      options.addOption(Option.builder().longOpt("id").argName("id").hasArg(true).desc("The ID of the form to export; required for form export").build());
      options.addOption(Option.builder().longOpt("import").argName("import").hasArg(false).desc("Import from file. Requires --file").build());
      options.addOption(Option.builder().longOpt("file").argName("file").hasArg(true).desc("File containing exported form data. Required if --import is set.").build());
      options.addOption(Option.builder().longOpt("skip-conditions").argName("skip-conditions").hasArg(false).desc("If set, skip linking the form to conditions").build());
      options.addOption(Option.builder().longOpt("skip-agencies").argName("skip-agencies").hasArg(false).desc("If set, skip linking the form to agencies").build());
      options.addOption(Option.builder().longOpt("dry-run").argName("dry-run").hasArg(false).desc("Don't actually commit anything. Useful only for --import mode.").build());
      options.addOption(Option.builder().longOpt("help").argName("help").hasArg(false).desc("Show help").build());
      CommandLineParser clp = new DefaultParser();
      cmd = clp.parse(options, args);
    } catch (org.apache.commons.cli.ParseException e) {
      System.err.println(e.getMessage());
      printHelp(options);
    }
    if (cmd != null && cmd.hasOption("help")) {
      printHelp(options);
    }
    if (cmd.hasOption("import") && !cmd.hasOption("file")) {
      System.err.println("--import mode requires --file");
      printHelp(options);
    }
    return cmd;
  }
}
