Tool to export and import formbuilder forms in Epitrax.

BUILDING:
`mvn clean package`

This builds a .jar file complete with all necessary libraries, suitable for
`java -jar ...`, as well as a .jar free from those libraries. Both are found in
the `target/` subdirectory.

USAGE:
java -jar form-exporter.jar --user USER --pass PASS --id ID
            [--help] [--url jdbc:postgresql://host:port/database]
    --help          Show help
    --id <id>       The ID of the form to export; required
    --pass <pass>   The database password; required
    --url <url>     The JDBC URL for the database to use. Default:
                    jdbc:postgresql://localhost:5432/nedss
    --user <user>   The database username; required

Exports a form to STDOUT, in JSON. The code to import the form again isn't done yet.
